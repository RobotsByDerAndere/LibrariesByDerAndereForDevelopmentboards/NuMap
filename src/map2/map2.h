/*
 * @title: map2.h
 * @version: 2.1
 * @author: DerAndere
 * @created: 2018
 * SPDX-License-Identifier: MIT
 * Copyright 2018 DerAndere
 * @license: Apache-2.0
 * @info: http://www.it-by-derandere.blogspot.com
 * @language: C++
 * @about: Library for mapping values between an initial minimum and maximum
 * limit to a target range of values between an output minimum and maximum.
 * Intermediate values are linearly interpolated. Replaces the flawed map() function
 * of Arduino.h. Inspired by a forum post by Bill Perry aka bperrybap from 2015:
 * http://forum.arduino.cc/index.php?topic=417690.msg2877460#msg2877460.
 */

#ifndef map2_H
#define map2_H


#include <Arduino.h>

namespace map2 {

/**
 * map2 Function.
 * @param: long initial_val (between initial_min and initial_max)
 * @param: long initial_min
 * @param: long initial_max
 * @param: long final_min
 * @param: long final_max
 * @return: long (final_min-final_max)
 * @fn: Inspired by a forum post by Bill Perry aka bperrybap from 2015:
 * http://forum.arduino.cc/index.php?topic=417690.msg2877460#msg2877460.
 * Description: maps in equal intervals (using linear interpolation) as opposed to
 * flawed map() function from the Arduino.h library.
 */
    long map2(long initial_val, long initial_min, long initial_max, long final_min, long final_max);

}

#endif /* map2_H */
