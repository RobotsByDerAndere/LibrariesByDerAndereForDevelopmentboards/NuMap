/*
 * @title: map2.cpp
 * @version: 2.1
 * @author: DerAndere
 * @created: 2018
 * SPDX-License-Identifier: Apache-2.0
 * Copyright 2018 DerAndere
 * @license: MIT
 * @info: http://www.it-by-derandere.blogspot.com
 * @language: C++
 * @about: Library for mapping values between an initial minimum and maximum
 * limit to a target range of values between an output minimum and maximum.
 * Intermediate values are linearly interpolated. Replaces the flawed map() function
 * of Arduino.h. Inspired by a forum post by Bill Perry aka bperrybap from 2015:
 * http://forum.arduino.cc/index.php?topic=417690.msg2877460#msg2877460.
 */

#include <Arduino.h>
#include "map2.h"

namespace map2 {

/**
 * map2 Function
 * Inspired by a forum post by Bill Perry aka bperrybap from 2015:
 * http://forum.arduino.cc/index.php?topic=417690.msg2877460#msg2877460.
 * Description: maps in equal intervals (using linear interpolation) as opposed to
 * flawed map() function from the Arduino.h library.
 */

long map2(long initial_val, long initial_min, long initial_max, long final_min, long final_max)
{
  if(initial_val == initial_max)
     return final_max;
   else if(final_min < final_max)
     return (initial_val - initial_min) * (final_max - final_min+1) / (initial_max - initial_min) + final_min;
   else
     return (initial_val - initial_min) * (final_max - final_min-1) / (initial_max - initial_min) + final_min;
}

}
