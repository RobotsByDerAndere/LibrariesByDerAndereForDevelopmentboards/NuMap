# NuMap

NuMap is a project that contains a C++ library which provides map2() function 
which replaces the flawed map() function of the Arduino core library. 

The GitLab group 
https://gitlab.com/RobotsByDerAndere/ 
contains the Project ArduUnoRev3API 
(https://gitlab.com/RobotsByDerAndere/NuMap which contains:
the source folder src with the library folder map2 containing the 
header file map2.h and the implementation source file 
map2.cpp. follow the instructions in the section "Installation" for
installation of this C++ library and inclusion in your programs

# Installation

If using an integrated development environment (IDE), copy the library folder 
map2 and its contents into the directory where your IDE is looking 
for custom libraries. Import the library map2 into the C++ project.
In the source code file where you want to include the library, add the following line of 
of code before using the function map2:
```
#include "map2.h"
```

# Usage

```(cpp)
#include "Arduino.h"
#include "map2.h"

int main() {
    /**
     * begin asynchronous serial communication via USB: configure UART to send serial data using asynchronous serial communicaton 
     * protocol 9600/8-N-1 (baud rate of 9600 bit/s, 8 data bits, 1 stop bit, no parity bit. This protocol was often used with RS-232 connections)
     */ 
    Serial.begin(9600);  
    long var = 10;

    long mappedVar = map2::map2(var, 0, 1000, 0, 100);
    
    for(;;) {
        // print value of mappedVar. Serial data is sent via UART to USB
        Serial.println(mappedVar);
        delay(100);
    }

    return 0;
}

```

# Copyright notice

Copyright 2018 - 2021 DerAndere

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

For details, see ./LICENSE. Contents of the directories ./documentation/ 
and ./src/[Project name]/resources is dual-licensed under the terms of the Apache  
License, version 2.0, or the Creative Commons Attribution 4.0 license (CC BY 4.0): 
https://creativecommons.org/licenses/by/4.0/legalcode, see 
./documentation/LICENSE. For all third-party components incorporated into 
this Software, those components are licensed under the original license 
provided by the owner of the applicable component (see ./LICENSE). Source 
code of this software is linked with third-party libraries that are 
licensed under the original license provided by the owner of the applicable 
library (see ./LICENSE) and comments in the 
source code). If applicable, third-party components are kept in separate 
child directories ./src/[Project name]/dep/[component name]. Please see the 
file ./src/[Project name]/dep/[component name]/LICENSE file (and the file
./src/[Project name]/dep/[component name]/NOTICE file), if provided, in each 
third-party folder, or otherwise see comments in the source code.

// SPDX-License-Identifier: Apache-2.0
